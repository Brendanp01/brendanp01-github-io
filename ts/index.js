"use strict";
var _a;
function toggleDarkMode() {
    console.log("Dark mdoe toggled");
    if (document.querySelector("#darkToggle").innerHTML == "light_mode") {
        document.querySelector("#darkToggle").innerHTML = "dark_mode";
    }
    else if (document.querySelector("#darkToggle").innerHTML == "dark_mode") {
        document.querySelector("#darkToggle").innerHTML = "light_mode";
    }
}
window.onload = toggleDarkMode();
(_a = document.querySelector("#darkToggle")) === null || _a === void 0 ? void 0 : _a.addEventListener("click", toggleDarkMode);
